/**
 * Created by java on 10.10.2016.
 */
public class Account {
    private Long accountNumber;
    private Client client;
    private Double balance;
    long GLOBAL_ACCOUNT_NUMBER = 1_000_000L;

    public Long getAccountNumber() {
        return accountNumber;
    }

    public Client getClient() {
        return client;
    }

    public Double getBalance() {
        return balance;
    }

    public void setAccountNumber(Long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Account(Long accountNumber, Client client, Double balance, long GLOBAL_ACCOUNT_NUMBER) {
        this.accountNumber = GLOBAL_ACCOUNT_NUMBER;
        this.client = client;
        this.balance = balance;
        GLOBAL_ACCOUNT_NUMBER++;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.accountNumber == accountNumber) return true;
        return true;
    }

    @Override
    public int hashCode() {
        int rezalt = accountNumber.hashCode();
        return rezalt;
    }

    @Override
    public String toString() {

        System.out.print("Account " + accountNumber + "  , client=" + client + " , balance =" + balance);
    }
}
