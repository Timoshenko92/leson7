import java.util.Date;

/**
 * Created by java on 10.10.2016.
 */
public class Client {
        private String firstName;
        private String lastName;
        private Date dateOfBirth;
        private String address;
        private String phone;

    public Client( String firstName, String lastName, Date dateOfBirth, String address, String phone){
        this.firstName=firstName;
        this.lastName=lastName;
        this.dateOfBirth=dateOfBirth;
        this.address=address;
        this.phone=phone;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.firstName==firstName)return true;
        if (this.lastName==lastName)return true;
        if (this.dateOfBirth==dateOfBirth)return true;
        if (this.address==address)return true;
        return true;
    }

    @Override
    public int hashCode() {
        int rezalt=firstName.hashCode();
        rezalt=31*rezalt+lastName.hashCode();
        rezalt=31*rezalt+dateOfBirth.hashCode();
        rezalt=31*rezalt+address.hashCode();

        return rezalt;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public Date getDateOfBirth() {
        return dateOfBirth;
    }
    public String getAddress() {
        return address;
    }
    public String getPhone() {
        return phone;
    }


}
